﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace GettingStartedOutlookAddIn
{
    public partial class Ribbon1
    {
        MAPIFolder mailsFromThisFolder = null;
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                Outlook.Application application = Globals.ThisAddIn.Application;

                Outlook.Explorer currExplorer = application.ActiveExplorer();

                //MAPIFolder mainFolder = Globals.ThisAddIn.Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
                MAPIFolder mainFolder = currExplorer.CurrentFolder;

                var dictionary = new Dictionary<string, int>();
                var listBefore = new List<string>();
                var afterBefore = new List<string>();
                string before = "";  string after = "";

                foreach (Outlook.MailItem mailItem in mainFolder.Items)
                {
                    if (mailItem != null)
                    {
                        for (int i = 1; i <= mailItem.UserProperties.Count; i++)
                        {
                            var userProperty = mailItem.UserProperties[i];
                            if (userProperty.Name == "Domain")
                            {
                                try
                                {
                                    var str = mailItem.SenderEmailAddress.ToString();
                                    MailAddress address = new MailAddress(str);
                                    string host = address.Host;

                                    if (dictionary.ContainsKey(host))
                                        dictionary[host]++;
                                    else
                                        dictionary.Add(host, 1);

                                    listBefore.Add(host);
                                    before += host + " ::::: /n ::::: ";
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }

                var items = mainFolder.Items;
                items.Sort("[Domain]", false);

                foreach (Outlook.MailItem mailItem in mainFolder.Items)
                {
                    if (mailItem != null)
                    {
                        for (int i = 1; i <= mailItem.UserProperties.Count; i++)
                        {
                            var userProperty = mailItem.UserProperties[i];
                            if (userProperty.Name == "Domain")
                            {
                                try
                                {
                                    var str = mailItem.SenderEmailAddress.ToString();
                                    MailAddress address = new MailAddress(str);
                                    string host = address.Host;

                                    afterBefore.Add(host);
                                    after += host + " ::::: /n ::::: ";
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }


                MessageBox.Show("unsort: " + before);
                MessageBox.Show("sort: " + after);

                var sortedDict = (from entry in dictionary orderby entry.Value descending select entry).ToList();
                foreach (var keyVal in sortedDict)
                {
                    MessageBox.Show("Distonary: " + keyVal.Key + "&" + keyVal.Value);
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }


            //Outlook.Views views = null;
            //Outlook.TableView tableView = null;
            //Outlook.ViewFields viewFields = null;
            //Outlook.ViewField viewField = null;
            //Outlook.ColumnFormat columnFormat = null;

            //// get explorer instance 
            //Outlook.Application application = Globals.ThisAddIn.Application;
            //Outlook.Explorer explorer = application.ActiveExplorer();
            //Outlook.Explorer explorerInstance = explorer as Outlook.Explorer;

            //// get current folder 
            //Outlook.MAPIFolder folder = null;
            //folder = explorerInstance.CurrentFolder;

            //// make sure folder is a tableView 
            //if (folder != null && folder.CurrentView.ViewType == Outlook.OlViewType.olTableView)
            //{
            //    if (folder.Items != null && folder.Items.Count > 0)
            //    {
            //        //foreach(var item in folder.Items)
            //        //{
            //        //    MessageBox.Show("item" + item);
            //        //}
            //        if (folder.Items[1] is Outlook._MailItem)
            //        {
            //            //mailItem = folder.Items[1] as Outlook._MailItem;

            //            // now I add the property to this mail Item if it has not already been added using code similar to this: 
            //            // SEE link at bottom of post 

            //            // finally I set the current view of the folder to show this new column 
            //            tableView = folder.CurrentView as Outlook.TableView;
            //            viewField = tableView.ViewFields.Add("DomainCount");
            //            columnFormat = viewField.ColumnFormat;
            //            columnFormat.Align = Outlook.OlAlign.olAlignLeft;
            //            columnFormat.Width = 10;
            //            tableView.Save();
            //            tableView.Apply();
            //        }
            //    }

            //}


            //Outlook.Explorer currExplorer = null;

            //Outlook.MAPIFolder currFolder = null;

            //Outlook.Views views = null;

            //Outlook.TableView tableView = null;

            //Outlook.ViewFields viewFields = null;

            //Outlook.ViewField viewField = null;

            //Outlook.ColumnFormat columnFormat = null;

            //Outlook.UserDefinedProperties userProperties = null;

            //Outlook.UserDefinedProperty billableProperty = null;

            //try

            //{
            //    Outlook.Application application = Globals.ThisAddIn.Application;


            //    currExplorer = application.ActiveExplorer();

            //    currFolder = currExplorer.CurrentFolder;

            //    userProperties = currFolder.UserDefinedProperties;

            //    //billableProperty = userProperties.Add("NewColumn", Outlook.OlUserPropertyType.olYesNo);
            //    billableProperty = userProperties.Find("DomainCount");

            //    views = currFolder.Views;

            //    views.Remove("My Custom View");

            //    tableView = (Outlook.TableView)views.Add("My Custom View", Outlook.OlViewType.olTableView, Outlook.OlViewSaveOption.olViewSaveOptionThisFolderEveryone);

            //    viewFields = tableView.ViewFields;

            //    viewField = tableView.ViewFields.Add("DomainCount");

            //    columnFormat = viewField.ColumnFormat;

            //    columnFormat.Align = Outlook.OlAlign.olAlignCenter;

            //    tableView.Save();

            //    tableView.Apply();

            //}

            //finally

            //{

            //    //if (userProperties != null)

            //    //    Marshal.ReleaseComObject(userProperties);

            //    //if (billableProperty != null)

            //    //    Marshal.ReleaseComObject(billableProperty);

            //    if (viewFields != null)

            //        Marshal.ReleaseComObject(viewFields);

            //    if (viewField != null)

            //        Marshal.ReleaseComObject(viewField);

            //    if (columnFormat != null)

            //        Marshal.ReleaseComObject(columnFormat);

            //    if (tableView != null)

            //        Marshal.ReleaseComObject(tableView);

            //    if (views != null)

            //        Marshal.ReleaseComObject(views);

            //    if (currFolder != null)

            //        Marshal.ReleaseComObject(currFolder);

            //    if (currExplorer != null)

            //        Marshal.ReleaseComObject(currExplorer);

            //}

        }


        public void GetFolders(MAPIFolder folder)
        {
            if (folder.Folders.Count == 0)
            {
                //if (folder.Name == "Folder Name")
                //{
                    //Console.WriteLine(m.FullFolderPath);
                    mailsFromThisFolder = folder;
                //}
            }
            else
            {
                foreach (MAPIFolder subFolder in folder.Folders)
                {
                    GetFolders(subFolder);
                }
            }
        }
    }
}
