﻿using Outlook = Microsoft.Office.Interop.Outlook;
using Word = Microsoft.Office.Interop.Word;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System;

namespace GettingStartedOutlookAddIn
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            // Get the Application object
            Outlook.Application application = this.Application;

            // Get the Inspectors objects
            Outlook.Inspectors inspectors = application.Inspectors;

            // Get the active Inspector
            Outlook.Inspector activeInspector = application.ActiveInspector();
            if (activeInspector != null)
            {
                // Get the active item's title when Outlook start
                MessageBox.Show("Active Inspector: " + activeInspector.Caption);
            }

            // Get the Explorers objects
            Outlook.Explorers explorers = application.Explorers;

            // Get the active Explorer object
            Outlook.Explorer activeExplorer = application.ActiveExplorer();
            Outlook.Selection activeSelection = activeExplorer.Selection;
            foreach(var obj in activeSelection)
            {
                //if (activeSelection != null)
                //{
                    MessageBox.Show("Active selction: " + obj);
                //}
            }
            
            if (activeExplorer != null)
            {
                // Get the active folder's title when Outlook start
                MessageBox.Show("Active Explorer: " + activeExplorer.Caption);
            }

            // shoaib
            Outlook.MAPIFolder inbox = Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
            inbox.Items.ItemAdd += new Outlook.ItemsEvents_ItemAddEventHandler(InboxFolderItemAdded);

            // Add a new Inspector to the application
            inspectors.NewInspector += 
                new Outlook.InspectorsEvents_NewInspectorEventHandler(
                    Inspectors_AddTextToNewMail);

            // Subscribe to the ItemSend event, that it's triggered when an email is sent
            application.ItemSend += 
                new Outlook.ApplicationEvents_11_ItemSendEventHandler(
                    ItemSend_BeforeSend);

            application.ItemLoad += Application_ItemLoad;

            // Add a new Inspector to the application
            inspectors.NewInspector += 
                new Outlook.InspectorsEvents_NewInspectorEventHandler(
                    Inspectors_RegisterEventWordDocument);
        }

        private void InboxFolderItemAdded(object Item)
        {
            if (Item is Outlook.MailItem)
            {
                // New mail item in inbox folder 
                MessageBox.Show("you got mail");
            }
        }

        private void Application_ItemLoad(object Item)
        {
            try
            {
                Outlook.Application application = Globals.ThisAddIn.Application;

                Outlook.Explorer currExplorer = application.ActiveExplorer();

                //MAPIFolder mainFolder = Globals.ThisAddIn.Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
                MAPIFolder mainFolder = currExplorer.CurrentFolder;
                var dictionary = new Dictionary<string, int>();
                
                foreach (Outlook.MailItem mailItem in mainFolder.Items)
                {
                    if (mailItem != null)
                    {
                        var isPresent = false;
                        for (int i = 1; i <= mailItem.UserProperties.Count; i++)
                        {
                            ////var list = new List<KeyValuePair<string, int>>();
                            var userProperty = mailItem.UserProperties[i];
                            //if (userProperty.Name == "DomainCount")
                            //{
                            //    //MessageBox.Show("Already: " + userProperty.Name + "&" + userProperty.Value);
                            //}
                            if (userProperty.Name == "Domain")
                            {
                                //list.Add(new KeyValuePair<string, int>("Cat", 1));
                                isPresent = true;
                                var str = mailItem.SenderEmailAddress.ToString();
                                MailAddress address = new MailAddress(str);
                                string host = address.Host;

                                if (dictionary.ContainsKey(host))
                                {
                                    dictionary[host]++;
                                }
                                else
                                {
                                    dictionary.Add(host, 1);
                                    ////MessageBox.Show("Distonary: " + keyVal.Key + "&" + keyVal.Value);
                                    //Outlook.UserProperties mailUserProperties = null;
                                    //Outlook.UserProperty mailUserProperty = null;

                                    //mailUserProperties = mailItem.UserProperties;
                                    //mailUserProperty = mailUserProperties.Add("DomainCount",
                                    //   Outlook.OlUserPropertyType.olText, false, 1);
                                    //// Where 1 is OlFormatText (introduced in Outlook 2007)
                                    //mailUserProperty.Value = dictionary.ContainsKey(host);
                                    //mailItem.Save();
                                }

                                //MessageBox.Show("Already: " + userProperty.Name + "&" + userProperty.Value);
                            }
                        }
                            if(!isPresent)
                            {
                                Outlook.UserProperties mailUserProperties = null;
                                Outlook.UserProperty mailUserProperty = null;
                                    var str = mailItem.SenderEmailAddress.ToString();

                                    MailAddress address = new MailAddress(str);
                                    string host = address.Host;

                                    mailUserProperties = mailItem.UserProperties;
                                    mailUserProperty = mailUserProperties.Add("Domain",
                                       Outlook.OlUserPropertyType.olText, false, 1);
                                    // Where 1 is OlFormatText (introduced in Outlook 2007)
                                    mailUserProperty.Value = host;
                                    mailItem.Save();
                            }

                        //var sortedDict = (from entry in dictionary orderby entry.Value descending select entry).ToList();
                        //foreach (var keyVal in dictionary)
                        //{
                        //    //MessageBox.Show("Distonary: " + keyVal.Key + "&" + keyVal.Value);
                        //    Outlook.UserProperties mailUserProperties = null;
                        //    Outlook.UserProperty mailUserProperty = null;

                        //    mailUserProperties = mailItem.UserProperties;
                        //    mailUserProperty = mailUserProperties.Add("DomainCount",
                        //       Outlook.OlUserPropertyType.olText, false, 1);
                        //    // Where 1 is OlFormatText (introduced in Outlook 2007)
                        //    mailUserProperty.Value = keyVal.Key + keyVal.Value;
                        //    mailItem.Save();
                        //}
                        //}                       
                    }
                }


                //var sortedDict = (from entry in dictionary orderby entry.Value descending select entry).ToList();
              
                foreach (var keyVal in dictionary)
                {                   
                    foreach (Outlook.MailItem mailItem in mainFolder.Items)
                    {
                        var str = mailItem.SenderEmailAddress.ToString();
                        MailAddress address = new MailAddress(str);
                        string host = address.Host;
                        if(host == keyVal.Key)
                        {
                            Outlook.UserProperties mailUserProperties = null;
                            Outlook.UserProperty mailUserProperty = null;

                            mailUserProperties = mailItem.UserProperties;
                            mailUserProperty = mailUserProperties.Add("DomainCount",
                               Outlook.OlUserPropertyType.olText, false, 1);
                            // Where 1 is OlFormatText (introduced in Outlook 2007)
                            var val = GetIntegerDigitCount(keyVal.Value);
                            //int length = keyVal.Value.ToString().Length;
                            //MessageBox.Show(length.ToString());
                            mailUserProperty.Value = val + "(" + keyVal.Key + ")";
                            mailItem.Save();
                        }
                    }
                        //MessageBox.Show("Distonary: " + keyVal.Key + "&" + keyVal.Value);
                }
            }
            catch
            {
                
            }
        }

        static string GetIntegerDigitCount(int valueInt)
        {
            //string space = "          ";
            //string s = "";
            //for(int i= 1; i <=2; i++ )
            //{
            //    s += " ";
            //}

            //return (space - s);

            double value = valueInt;
            //int sign = 0;
            //if (value < 0)
            //{
            //    value = -value;
            //    sign = 1;
            //}
            if (value <= 9)
            {
                return "         " + value;
                //return "*********" + value;
            }
            if (value <= 99)
            {
                return "        " + value;
                //return "********" + value;
            }
            if (value <= 999)
            {
                return "       " + value;
                //return "*******" + value;
            }
            if (value <= 9999)
            {
                return "      " + value;
                //return "******" + value;
            }
            if (value <= 99999)
            {
                return "     " + value;
                //return "*****" + value;
            }
            if (value <= 999999)
            {
                return "    " + value;
                //return "****" + value;
            }
            if (value <= 9999999)
            {
                return "   " + value;
                //return "***" + value;
            }
            if (value <= 99999999)
            {
                return "  " + value;
                //return "**" + value;
            }
            if (value <= 999999999)
            {
                return " " + value;
                //return "*" + value;
            }
            return "**********";
        }

        void ItemSend_BeforeSend(object item, ref bool cancel)
        {
            Outlook.MailItem mailItem = (Outlook.MailItem) item;
            if (mailItem != null)
            {
                mailItem.Body += "Modified by GettingStartedOutlookAddIn";
                Outlook.UserProperties mailUserProperties = null;
                Outlook.UserProperty mailUserProperty = null;
                //Outlook.MailItem mailItem = (Outlook.MailItem)Item;
                if (mailItem != null)
                {
                    var str = mailItem.SenderEmailAddress.ToString();
                    //string s1 = str.Substring(str.IndexOf("@"), str.Length - str.IndexOf("@") - 1);

                    mailUserProperties = mailItem.UserProperties;
                    mailUserProperty = mailUserProperties.Add("Domain",
                       Outlook.OlUserPropertyType.olText, true, 1);
                    // Where 1 is OlFormatText (introduced in Outlook 2007)
                    mailUserProperty.Value = str;
                    mailItem.Save();
                }
            }
            cancel = false;
        }

        void Inspectors_AddTextToNewMail(Outlook.Inspector inspector)
        {
            // Get the current item for this Inspecto object and check if is type
            // of MailItem
            Outlook.MailItem mailItem = inspector.CurrentItem as Outlook.MailItem;            
            if (mailItem != null)
            {
                if (mailItem.EntryID == null)
                {
                    mailItem.Subject = "My subject text";
                    mailItem.Body = "My body text";
                }
            }
        }

        void Inspectors_RegisterEventWordDocument(Outlook.Inspector inspector)
        {
            Outlook.MailItem mailItem = inspector.CurrentItem as Outlook.MailItem;
            if (mailItem != null)
            {
                // Check that the email editor is Word editor
                // Although "always" is a Word editor in Outlook 2013, it's best done perform this check
                if (inspector.EditorType == Outlook.OlEditorType.olEditorWord && inspector.IsWordMail())
                {
                    // Get the Word document
                    Word.Document document = inspector.WordEditor;
                    if (document != null)
                    {
                        // Subscribe to the BeforeDoubleClick event of the Word document
                        document.Application.WindowBeforeDoubleClick += 
                            new Word.ApplicationEvents4_WindowBeforeDoubleClickEventHandler(
                                ApplicationOnWindowBeforeDoubleClick);
                    }
                }
            }
        }

        private void ApplicationOnWindowBeforeDoubleClick(Word.Selection selection, ref bool cancel)
        {
            // Get the selected word
            Word.Words words = selection.Words;
            MessageBox.Show("Selection: " + words.First.Text);
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see http://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
